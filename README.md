## Insurance claims prediction

This repository makes use of the data provided by Allstate Claims Severity Kaggle competition. Here, we perform EDA and we evaluate a few models. In addition, we make use of seaborn python package to produce informative statistical graphics.

